RSpec.shared_context "setup bag product" do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:root_taxon) { taxonomy.root }
  let(:bag_taxon) { create(:taxon, name: 'Bags', taxonomy: taxonomy, parent: root_taxon) }
  let(:bag_variant) { create(:variant, images: [create(:image)], is_master: true) }
  let(:bag_product) { create(:product, name: "Test Bag", taxons: [bag_taxon], variants: [bag_variant]) }
end

RSpec.shared_context "setup bag,shirt,t-shirts products" do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:root_taxon) { taxonomy.root }
  let(:bag_taxon) { create(:taxon, name: 'Bags', taxonomy: taxonomy, parent: root_taxon) }
  let(:clothing_taxon) { create(:taxon, name: 'Clothing', taxonomy: taxonomy, parent: root_taxon) }
  let(:shirt_taxon) { create(:taxon, name: 'Shirts', taxonomy: taxonomy, parent: clothing_taxon) }
  let(:tshirt_taxon) { create(:taxon, name: 'T-Shirts', taxonomy: taxonomy, parent: clothing_taxon) }
  let(:bag_variant) { create(:variant, images: [create(:image)], is_master: true) }
  let(:shirt_variant) { create(:variant, images: [create(:image)], is_master: true) }
  let(:tshirt_variant_0) { create(:variant, images: [create(:image)], is_master: true) }
  let(:tshirt_variant_1) { create(:variant, images: [create(:image)], is_master: true) }
  let!(:bag_product) do
    create(:product, name: "Test Bag", variants: [bag_variant], taxons: [bag_taxon])
  end
  let!(:shirt_product) do
    create(:product, name: "Test Shirt", variants: [shirt_variant], taxons: [clothing_taxon, shirt_taxon])
  end
  let!(:tshirt_product_0) do
    create(:product, name: "Test T-Shirt_0", variants: [tshirt_variant_0], taxons: [clothing_taxon, tshirt_taxon])
  end
  let!(:tshirt_product_1) do
    create(:product, name: "Test T-Shirt_1", variants: [tshirt_variant_1], taxons: [clothing_taxon, tshirt_taxon])
  end
end
