require 'rails_helper'

RSpec.feature "Shared Templates", type: :feature do
  describe "_applicaton" do
    it "moves to application root when 'Home' was clicked" do
      visit potepan_root_path
      click_link "Home"
      expect(current_path).to eq potepan_root_path
    end

    it "moves to application root when navbar-brand was clicked" do
      visit potepan_root_path
      find("a.navbar-brand").click
      expect(current_path).to eq potepan_root_path
    end
  end
end
