require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  include_context "setup bag product"

  before do
    visit potepan_product_path bag_product.id
  end

  it "moves to application root when 'Home' link in breadcrumb is clicked" do
    within(:css, ".breadcrumb") do
      click_link "Home"
    end
    expect(current_path).to eq potepan_root_path
  end

  it "moves to category-wise produts list when '一覧ページへ戻る' link is clicked" do
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(bag_taxon.id)
  end
end
