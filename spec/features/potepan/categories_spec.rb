require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  describe "potepan::categories#show" do
    describe "root_taxon" do
      let(:taxonomy) { create(:taxonomy, name: "Categories") }
      let(:root_taxon) { taxonomy.root }

      before do
        visit potepan_category_path root_taxon.id
      end

      it "header title is equal to 'root_taxon.name - BASE_TITLE'" do
        expect(page).to have_title("#{root_taxon.name} - #{ApplicationHelper::BASE_TITLE}", exact: true)
      end

      it "moves to application root when 'Home' link in breadcrumb was clicked" do
        within(:css, ".breadcrumb") do
          click_link "Home"
        end
        expect(current_path).to eq potepan_root_path
      end
    end

    describe "bag_taxon" do
      include_context "setup bag product"

      before do
        visit potepan_category_path bag_product.taxons.first.id
      end

      it "header title is equal to 'bag_taxon.name - BASE_TITLE'" do
        expect(page).to have_title("#{bag_taxon.name} - #{ApplicationHelper::BASE_TITLE}", exact: true)
      end

      it "moves to product's page when product name was clicked" do
        click_link bag_product.name
        expect(current_path).to eq potepan_product_path(bag_product.id)
      end

      it "moves to product's page when product price was clicked" do
        click_link bag_product.master.display_amount.to_s
        expect(current_path).to eq potepan_product_path(bag_product.id)
      end
    end

    describe "product category page" do
      include_context "setup bag,shirt,t-shirts products"
      let(:product_boxes) { all(".productBox") }
      let(:product_captions) { all(".productCaption h5").map(&:text) }

      before do
        visit potepan_category_path root_taxon.id
      end

      it "shows correct '商品カテゴリ' side-bar" do
        expect(all("ul[id^='category-']").length).to eq 1
        expect(all("ul[id^='category-'] li").length).to eq 3

        expect(find("a[data-target='#category-#{taxonomy.id}']")).to have_text(taxonomy.name)
        within(:css, "#category-#{taxonomy.id}") do
          taxons = [bag_taxon, shirt_taxon, tshirt_taxon]
          expect(all("li").map(&:text)).to match_array(taxons.map { |taxon| "#{taxon.name} (#{taxon.products.size})" })
        end
      end

      it "shows correct bag list when sidebar product category is clicked" do
        n_products = bag_taxon.products.size
        click_link "#{bag_taxon.name} (#{n_products})"

        expect(product_boxes.length).to eq n_products
        expect(product_captions).to match_array([bag_product.name])
      end

      it "shows correct shirt list when sidebar product category is clicked" do
        n_products = shirt_taxon.products.size
        click_link "#{shirt_taxon.name} (#{n_products})"

        expect(product_boxes.length).to eq n_products
        expect(product_captions).to match_array([shirt_product.name])
      end

      it "shows correct t-shirts list when sidebar product category is clicked" do
        n_products = tshirt_taxon.products.size
        click_link "#{tshirt_taxon.name} (#{n_products})"

        expect(product_boxes.length).to eq n_products
        expect(product_captions).to match_array([tshirt_product_0.name, tshirt_product_1.name])
      end
    end
  end
end
