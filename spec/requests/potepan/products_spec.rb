require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "potepan::products#show" do
    include_context "setup bag product"

    before do
      get potepan_product_path bag_product.id
    end

    it "responds successfully if valid @product is specified" do
      expect(response).to have_http_status(200)
    end

    it "obtains a correct @product" do
      expect(controller.instance_variable_get("@product")).to eq bag_product
    end

    it "contains a correct product name" do
      expect(response.body).to include bag_product.name
    end

    it "contains a correct product description" do
      expect(response.body).to include bag_product.description
    end

    it "contains a correct product price" do
      expect(response.body).to include bag_product.master.display_amount.to_s
    end
  end
end
