require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "potepan::categories#show" do
    include_context "setup bag,shirt,t-shirts products"
    let(:page_html) { Nokogiri::HTML(response.body) }
    let(:categories) { page_html.css("ul[id^='category-'] li").map(&:text) }
    let(:product_boxes) { page_html.css(".productBox") }

    describe "root taxon" do
      before do
        get potepan_category_path root_taxon.id
      end

      it "responds successfully if root_taxon is specified" do
        expect(response).to have_http_status(200)
      end

      it "obtains a correct taxonomies" do
        expect(controller.instance_variable_get("@taxonomies")).to match_array([taxonomy])
      end

      it "obtains a correct root_taxon" do
        expect(controller.instance_variable_get("@taxon")).to eq root_taxon
      end

      it "obtains a correct products" do
        products = [bag_product, shirt_product, tshirt_product_0, tshirt_product_1]
        expect(controller.instance_variable_get("@products")).to match_array(products)
      end

      it "contains a correct taxon names" do
        expect(response.body).to include bag_taxon.name
        expect(response.body).to include shirt_taxon.name
        expect(response.body).to include tshirt_taxon.name
        expect(response.body).not_to include clothing_taxon.name
      end

      it "contains a correct product names" do
        expect(response.body).to include bag_product.name
        expect(response.body).to include shirt_product.name
        expect(response.body).to include tshirt_product_0.name
        expect(response.body).to include tshirt_product_1.name
      end

      it "satisfies #(products in the sidebar) is equal to #(products actually displayed)" do
        expect(categories.map { |c| c.scan(/\d+/).last.to_i }.sum).to eq product_boxes.length
      end
    end

    describe "t-shirt taxon" do
      before do
        get potepan_category_path tshirt_taxon.id
      end

      it "responds successfully if t-shirt taxon is specified" do
        expect(response).to have_http_status(200)
      end

      it "contains only t-shirt names" do
        expect(response.body).not_to include bag_product.name
        expect(response.body).not_to include shirt_product.name
        expect(response.body).to include tshirt_product_0.name
        expect(response.body).to include tshirt_product_1.name
      end

      it "satisfies #(products in the sidebar) is equal to #(products actually displayed)" do
        category_text = categories.find { |c| c.match(/#{tshirt_taxon.name}/) }
        expect(category_text.scan(/\d+/).last.to_i).to eq product_boxes.length
      end
    end
  end
end
