class Potepan::CategoriesController < ApplicationController
  TAXON_ID = "spree_products_taxons.taxon_id".freeze

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(taxons: :products)
    @products = Spree::Product.includes(master: [:images, :default_price]).in_taxon(@taxon).reorder(TAXON_ID)
  end
end
